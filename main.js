const { app, BrowserWindow } = require('electron')
const electronLocalshortcut = require('electron-localshortcut');
const axios = require('axios');
const wifiName = require('wifi-name');


// Gardez une reference globale de l'objet window, si vous ne le faites pas, la fenetre sera
// fermee automatiquement quand l'objet JavaScript sera garbage collected.
let win

function createWindow () {
    // Créer le browser window.
    win = new BrowserWindow({ width: 800, height: 600 })

    // et charge le index.html de l'application.
    win.loadFile('index.html')

    // Ouvre les DevTools.
    // win.webContents.openDevTools()

    goProThings()

    // Émit lorsque la fenêtre est fermée.
    win.on('closed', () => {
        // Dé-référence l'objet window , normalement, vous stockeriez les fenêtres
        // dans un tableau si votre application supporte le multi-fenêtre. C'est le moment
        // où vous devez supprimer l'élément correspondant.
        win = null
    })
}

function goProThings() {
    electronLocalshortcut.register(win, 'F12', () => {
        // Ouvre les DevTools.
        win.webContents.openDevTools()
    });
    // On "R" press
    electronLocalshortcut.register(win, 'R', () => {
        // Open DevTools
        // win.webContents.openDevTools()
        // Get camera status
        axios.get('http://10.5.5.9/gp/gpControl/status').then(function (response) {
            console.log(response.data.status['8']);
            // If status is recording
            if(response.data.status['8'] == '1'){
                // Stop recording
                axios.get('http://10.5.5.9/gp/gpControl/command/shutter?p=0').then(function (response) {
                    console.log(response);
                });
            }
            else {
                // Record
                axios.get('http://10.5.5.9/gp/gpControl/command/shutter?p=1').then(function (response) {
                    console.log(response);
                });
            }
        });

    });

    // On "M" press
    electronLocalshortcut.register(win, 'M', () => {
        // Go Video Mode
        axios.get('http://10.5.5.9/gp/gpControl/command/mode?p=0').then(function (response) {
            console.log(response);
            // Go Video 1080p Mode
            axios.get('http://10.5.5.9/gp/gpControl/setting/2/9').then(function (response) {
                console.log(response);
                // Go 30 FPS Mode
                axios.get('http://10.5.5.9/gp/gpControl/setting/3/8').then(function (response) {
                    console.log(response);
                });
            });
        });
    });

    // On "W" press
    electronLocalshortcut.register(win, 'W', () => {
        wifiName().then(name => {
            console.log(document)
            document.getElementById('wifiname').textContent(name)
        	console.log(name);
        	//=> 'wu-tang lan'
        });
    });
}

// Cette méthode sera appelée quant Electron aura fini
// de s'initialiser et sera prêt à créer des fenêtres de navigation.
// Certaines APIs peuvent être utilisées uniquement quand cet événement est émit.
app.on('ready', createWindow)

// Quitte l'application quand toutes les fenêtres sont fermées.
app.on('window-all-closed', () => {
    // Sur macOS, il est commun pour une application et leur barre de menu
    // de rester active tant que l'utilisateur ne quitte pas explicitement avec Cmd + Q
    if (process.platform !== 'darwin') {
        app.quit()
    }
})

app.on('activate', () => {
    // Sur macOS, il est commun de re-créer une fenêtre de l'application quand
    // l'icône du dock est cliquée et qu'il n'y a pas d'autres fenêtres d'ouvertes.
    if (win === null) {
        createWindow()
    }
})

// Dans ce fichier, vous pouvez inclure le reste de votre code spécifique au processus principal. Vous pouvez également le mettre dans des fichiers séparés et les inclure ici.
